/// <reference path="Interfaces.ts"/>
/// <reference path="Core.ts"/>

module _graphlib {
	var _debug = false;
	
	export class Tiles {
		_tiles: Tile[];
		viewport: HTMLElement;
		size: Vec2;
		offset: Vec2;
		tileSize: Vec2;
		tileCount: Vec2;
		tileAttributes: StringDictionary;
		tileUpdateCallback: Function;

		constructor (viewport: HTMLElement, tileUpdateCallback: Function) {
			if (this instanceof Tiles === false) throw "Use new instead of calling this function";

			// Tile array
			this._tiles = [];

			// Container for tile elements
			this.viewport = viewport;
			this.size = { x: viewport.offsetWidth, y: viewport.offsetHeight };

			// Current offset for tile scrolling
			this.offset = { x: 0, y: 0 };

			// Tile element
			this.tileAttributes = { "class": "tile" };

			// Tile size
			this.tileSize = getTileSize(this);
			if (this.tileSize.x < 1) throw "Tile width is less than 1";
			if (this.tileSize.y < 1) throw "Tile height is less than 1";

			// Tile count
			this.tileCount = { 
				x: Math.ceil(this.size.x / this.tileSize.x + 1),
				y: Math.ceil(this.size.y / this.tileSize.y + 1)
			};

			// Update callback
			this.tileUpdateCallback = tileUpdateCallback;
			
			// Create
			createTiles(this);
		}

		public redrawTiles() {
			if (!this.tileUpdateCallback) return;

			var tile;

			for (var t in this._tiles) {
				tile = this._tiles[t];
				this.tileUpdateCallback(tile);
			}
		}

		public updateTiles(delta: Vec2) {
			// Update current tile offset
			this.offset.x -= delta.x;
			this.offset.y += delta.y;

			var tile: Tile;
			var wrap = { x: 0, y: 0 };

			// Update tiles
			for (var t in this._tiles) {
				tile = this._tiles[t];
				var updateNeeded = false;

				// Update relative tile position
				Vector.add(tile.position, delta, tile.position);

				// Wrap tiles on X axis
				wrap.x = getWrappedValue(delta.x, tile.position.x, this.tileCount.x, this.tileSize.x, this.size.x);
				if (wrap.x !== tile.position.x) {
					tile.position.x = wrap.x;
					tile.coordinates.x -= Math.sign(delta.x) * this.tileSize.x * this.tileCount.x;
					updateNeeded = true;
				}

				// Wrap tiles on Y axis
				wrap.y = getWrappedValue(delta.y, tile.position.y, this.tileCount.y, this.tileSize.y, this.size.y);
				if (wrap.y !== tile.position.y) {
					tile.position.y = wrap.y;
					tile.coordinates.y += Math.sign(delta.y) * this.tileSize.y * this.tileCount.y;
					updateNeeded = true;
				}

				// Call update
				if (updateNeeded && this.tileUpdateCallback) 
					this.tileUpdateCallback(tile);

				// Update tile element
				tile.element.style.left = tile.position.x + "px";
				tile.element.style.top = tile.position.y + "px";
			}
		}

		public addTile(tile: Tile) {
			// Add tile to array
			this._tiles.push(tile);
			
			// Add element to viewport
			this.viewport.appendChild(tile.element);
		}
	}
	
	function getWrappedValue(delta: number, pos: number, tileCount: number, tileSize: number, portSize: number): number {
		var totalSize, moves;

		// Moving tile right, wrap to left end
		if (delta > 0 && pos > portSize) {
			totalSize = tileCount * tileSize;
			moves = (pos - portSize) / totalSize;
			return pos - Math.ceil(moves) * totalSize;
		}
		
		// Moving tile left, wrap to right end
		if (delta < 0 && pos + tileSize <= 0) {
			totalSize = tileCount * tileSize;
			moves = (Math.abs(pos) + tileSize + portSize) / totalSize;
			return pos + Math.floor(moves) * totalSize;
		}

		// No wrap needed
		return pos;
	}

	// Create temp tile to get size from CSS
	function getTileSize(tiles: Tiles): Vec2 {
		// Create div
		var div = document.createElement("div");

		// Set attributes
		for (var a in tiles.tileAttributes) 
			div.setAttribute(a, tiles.tileAttributes[a]);

		// Add to viewport
		tiles.viewport.appendChild(div);
		
		// Get size
		var size = {
			x: div.offsetWidth, 
			y: div.offsetHeight
		};

		// Remove from viewport and return size		
		tiles.viewport.removeChild(div);
		return size;
	}

	// Create tiles
	function createTiles(tiles: Tiles) {
		// Offset tiles relative to bottom left
		var offset = { x: 0, y: 0 };
		offset.y = -tiles.tileSize.y + (tiles.size.y % tiles.tileSize.y) - tiles.tileSize.y;
		
		// Create tiles
		for (var x = 0; x < tiles.tileCount.x; x++) {
			for (var y = 0; y < tiles.tileCount.y; y++) {
				// Calculate tile position
				var pos = {
					x: x * tiles.tileSize.x + offset.x,
					y: y * tiles.tileSize.y + offset.y
				};

				// Calculate tile coordinates relative to bottom left
				var coords = Vector.clone(pos);
				coords.y = Math.abs(coords.y - tiles.size.y + tiles.tileSize.y);

				// Create element
				var ele = document.createElement("div");
				ele.addEventListener("mousedown", skipEvent, false);

				// Add attributes
				for (var a in tiles.tileAttributes) 
					ele.setAttribute(a, tiles.tileAttributes[a]);

				// Colour for debugging
				if (_debug) {
					var r = 128 + Math.floor(128 / (tiles.tileCount.x - 1)) * x;
					var g = 128 + Math.floor(128 / (tiles.tileCount.y - 1)) * y;
					var b = 192;
					ele.setAttribute("style", "background-color:rgba({0}, {1}, {2}, 0.5);".format(r, g, b));
				}

				// Set position
				ele.style.left = pos.x + "px";
				ele.style.top = pos.y + "px";

				// Create tile
				var tile = {
					coordinates: coords,
					position: pos,
					element: ele
				};

				// Add to tiles
				tiles.addTile(tile);
			}
		}
	}

	function skipEvent(e: MouseEvent) {
		e.preventDefault();
	}
}
