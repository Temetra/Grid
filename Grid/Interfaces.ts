interface String {
	format;
}

interface Window {
	requestAnimFrame;
	webkitRequestAnimationFrame;
	mozRequestAnimationFrame;
	oRequestAnimationFrame;
}

interface Math {
	sign;
}

interface Vec2 {
	x: number;
	y: number;
}

interface StringDictionary {
	[index: string]: string;
}

interface Tile {
	coordinates: Vec2;
	position: Vec2;
	element: HTMLElement;
}