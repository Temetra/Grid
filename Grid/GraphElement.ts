/// <reference path="Interfaces.ts"/>
/// <reference path="Core.ts"/>
/// <reference path="Tiles.ts"/>
/// <reference path="GridEffect.ts"/>

module _graphlib {
	export class GraphElement {
		tiles: Tiles;
		context: CanvasRenderingContext2D;
		queue: Tile[];

		constructor (viewport: HTMLElement) {
			if (this instanceof GraphElement === false) throw "Use new instead of calling this function";
			console.log("Creating tiles object");
			this.queue = [];
			this.tiles = new Tiles(viewport, tileUpdateCallback.bind(this));
			this.tiles.redrawTiles();
		}

		pan(delta: Vec2) {
			delta = Vector.clone(delta);
			window.requestAnimFrame(function() { this.tiles.updateTiles(delta); }.bind(this));
		}
	}
	
	function getOrCreateContext(): CanvasRenderingContext2D {
		if (this.context) return this.context;

		// Create context
		console.log("Creating context");
		var canvas = <HTMLCanvasElement>document.createElement("canvas");
		canvas.width = this.tiles.tileSize.x;
		canvas.height = this.tiles.tileSize.y;
		this.context = canvas.getContext("2d");
		return this.context;
	}

	function tileUpdateCallback(tile: Tile) {
		// Add to queue
		this.queue.push(tile);
		
		// Start processing
		if (this.queue.length === 1) window.requestAnimFrame(processQueue.bind(this));
	}

	function processQueue() {
		// Get context
		var context = getOrCreateContext.call(this);

		// Get next tile
		var tile = this.queue.shift();

		// Draw tile
		draw.call(this, tile, context);

		// Continue processing next frame
		if (this.queue.length > 0) window.requestAnimFrame(processQueue.bind(this));
	}

	function draw(tile: Tile, context) {
		// Clear
		context.clearRect(0, 0, this.tiles.tileSize.x, this.tiles.tileSize.y);

		// Draw fine grid
		gridEffect.context = context;
		gridEffect.width = 1;
		gridEffect.colour = "#ccc";
		gridEffect.step.x = gridEffect.step.y = 50;
		gridEffect.draw(tile.coordinates, { x: 25, y: 25 });

		// Draw main grid
		gridEffect.colour = "#666";
		gridEffect.draw(tile.coordinates);

		// Draw heavy grid
		gridEffect.width = 2;
		gridEffect.step.x = gridEffect.step.y = 200;
		gridEffect.draw(tile.coordinates);

		// Update tile
		var img = <HTMLImageElement>tile.element.firstChild;

		// Create img element
		if (img === null) {
			img = new Image(context.canvas.width, context.canvas.height);
			tile.element.appendChild(img);
		}

		// Set image src
		img.src = context.canvas.toDataURL("image/png");

		// Update text
		var txt = <HTMLDivElement>tile.element.children.item(1);

		// Create text element
		if (txt === null) {
			txt = <HTMLDivElement>document.createElement("div");
			txt.setAttribute("class", "txt");
			tile.element.appendChild(txt);
		}

		// Set text to coords
		txt.innerHTML = "({0}, {1})".format(tile.coordinates.x, tile.coordinates.y);
	}
}