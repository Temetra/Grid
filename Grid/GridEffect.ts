/// <reference path="Interfaces.ts"/>
/// <reference path="Core.ts"/>

module _graphlib.gridEffect {
	export var context: CanvasRenderingContext2D;
	export var width: number = 1;
	export var colour: string = "#000";
	export var step: Vec2 = { x: 50, y: 50 };
	export var scale: number = 1;
	var half: number;

	export function draw(coords: Vec2, offset: Vec2 = null) {
		if (context === null) return;

		// Set width and colour
		context.lineWidth = width;
		context.strokeStyle = colour;

		// Half-pixel offset to avoid aliasing
		half = context.lineWidth % 2 > 0 ? 0.5 : 0;
		
		// Draw vert
		if (step.x > 0) drawVerticalLines(coords.x, (offset ? offset.x : 0));

		// Draw horiz
		if (step.y > 0) drawHorizontalLines(coords.y, (offset ? offset.y : 0));
	}

	function drawVerticalLines(posx: number, offset: number) {
		// Scale
		var scaledStep = step.x * scale;

		// Calculate start position
		var x = Math.floor(posx / scale / step.x);
		x = x * scaledStep - posx;
		x += offset;

		// Draw lines
		var ox;

		while (x < context.canvas.width) {
			// Adjust for half-pixel
			ox = Math.round(x) + half;

			// Draw if on canvas
			if (ox >= 0) {
				context.beginPath();
				context.moveTo(ox, 0);
				context.lineTo(ox, context.canvas.height);
				context.stroke();
			}

			// Increment
			x += scaledStep;
		}
	}
	
	function drawHorizontalLines(posy: number, offset: number) {
		// Scale
		var scaledStep = step.y * scale;

		// Calculate start position
		var y = Math.floor(posy / scale / step.y);
		y = context.canvas.height - (y * scaledStep - posy) - 1;
		y -= offset;

		// Draw lines
		var oy;

		while (y >= 0) {
			// Adjust for half-pixel
			oy = Math.round(y) + half;

			// Draw if on canvas
			if (oy < context.canvas.height) {
				context.beginPath();
				context.moveTo(0, oy);
				context.lineTo(context.canvas.width, oy);
				context.stroke();
			}

			// Increment
			y -= scaledStep;
		}
	}
}